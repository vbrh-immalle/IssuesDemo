Dit is een voorbeeld van issues en het oplossen ervan in Gitlab.

Issues zijn een manier om discussiëren over potentiële bugs.
Eens een issue opgelost is, kan ze geclosed worden.

Door in de commit-message `Fix #1` of `Closes #1` toe te voegen,
wordt de issue automatisch gesloten.

Als je gaat kijken naar de 2 issues in deze repository, zie je dat:

- bij het closen van issue 1 heb ik dit manueel moeten doen en voor de
  duidelijkheid ook een link erbij gezet naar de commit dit dit oploste
- bij het oplossen van issue 2 heb ik simpelweg `Fix #2` gebruikt in de
  commit-message en de rest gebeurt automatisch! De issue krijgt ook een
  makkelijk aan te klikken link naar de commit in de welke het opgelost is.
  
> Gelieve zo veel mogelijk elke issue in een aparte commit op te lossen!